<?php

namespace DwiRengga\Org\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use HasFactory;

    public function __construct()
    {
        $this->table = config('struktur-organisasi.division.table_name');
    }

    protected $guarded = [];
}
