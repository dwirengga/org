<?php

namespace DwiRengga\Org\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkUnit extends Model
{
    use HasFactory;

    public function __construct()
    {
        $this->table = config('struktur-organisasi.work_unit.table_name');
    }
    
    protected $guarded = [];
}
