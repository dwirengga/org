<?php

Route::group(['namespace'=>'DwiRengga\Org\Http\Controllers'],function(){
	Route::resource('company', 'CompanyController');
	Route::resource('work-unit', 'WorkUnitController');
	Route::resource('division', 'DivisionController');
	Route::resource('position', 'PositionController');
});