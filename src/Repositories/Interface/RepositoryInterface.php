<?php

namespace DwiRengga\Org\Repositories\Interface;

use Illuminate\Http\Request;

interface RepositoryInterface
{
    public function all(Request $request);

    public function create(array $data);

    public function find($id);

    public function update($id, array $data);

    public function delete($id);
}