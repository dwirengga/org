<?php

namespace DwiRengga\Org\Repositories\Eloquent;

use DwiRengga\Org\Models\Position;

class PositionRepository extends BaseRepository
{
    public function __construct(Position $model){
        parent::__construct($model);
    }
}
