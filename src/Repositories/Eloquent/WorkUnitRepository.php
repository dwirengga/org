<?php

namespace DwiRengga\Org\Repositories\Eloquent;

use DwiRengga\Org\Models\WorkUnit;

class WorkUnitRepository extends BaseRepository
{
    public function __construct(WorkUnit $model){
        parent::__construct($model);
    }
}
