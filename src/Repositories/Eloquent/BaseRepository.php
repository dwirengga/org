<?php

namespace DwiRengga\Org\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;
use DwiRengga\Org\Repositories\Interface\RepositoryInterface;
use Illuminate\Http\Request;

class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct(Model $model){
        $this->model = $model;
    }

    public function all(Request $request)
    {
        $query = $this->model->orderBy($request->sort_by,$request->sort_dir);

        if($request->search){
            $query = $query->where('name','ilike','%'.$request->search.'%');
        }

        return $query->paginate($request->limit);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function update($id, array $data)
    {
        $query = $this->model->findOrFail($id);
        $query->update($data);
        return $query;
    }

    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }
}
