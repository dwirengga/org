<?php

namespace DwiRengga\Org\Repositories\Eloquent;

use DwiRengga\Org\Models\Division;

class DivisionRepository extends BaseRepository
{
    public function __construct(Division $model){
        parent::__construct($model);
    }
}
