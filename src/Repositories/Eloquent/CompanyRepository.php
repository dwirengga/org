<?php

namespace DwiRengga\Org\Repositories\Eloquent;

use DwiRengga\Org\Models\Company;

class CompanyRepository extends BaseRepository
{
    public function __construct(Company $model){
        parent::__construct($model);
    }
}
