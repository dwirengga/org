<?php

namespace DwiRengga\Org;

use Illuminate\Support\ServiceProvider;
use DwiRengga\Org\Repositories\Interface\RepositoryInterface;
use DwiRengga\Org\Repositories\Eloquent\CompanyRepository;
use DwiRengga\Org\Repositories\Eloquent\DivisionRepository;
use DwiRengga\Org\Repositories\Eloquent\PositionRepository;
use DwiRengga\Org\Repositories\Eloquent\WorkUnitRepository;
 
class StrukturOrganisasiServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../config/struktur-organisasi.php' => config_path('struktur-organisasi.php')], 'struktur-organisasi-config');
            $this->publishes([__DIR__.'/../database/migrations' => database_path('migrations')], 'struktur-organisasi-migrations');
            $this->publishes([__DIR__.'/routes' => base_path('routes/struktur-organisasi')], 'struktur-organisasi-routes');
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/struktur-organisasi.php', 'struktur-organisasi');

        $this->app->when(CompanyRepository::class)
            ->needs(RepositoryInterface::class)
            ->give(function ($app) {
                return new CompanyRepository();
            });

        $this->app->when(DivisionRepository::class)
            ->needs(RepositoryInterface::class)
            ->give(function ($app) {
                return new DivisionRepository();
            });

        $this->app->when(PositionRepository::class)
            ->needs(RepositoryInterface::class)
            ->give(function ($app) {
                return new PositionRepository();
            });

        $this->app->when(WorkUnitRepository::class)
            ->needs(RepositoryInterface::class)
            ->give(function ($app) {
                return new WorkUnitRepository();
            });
        }
}