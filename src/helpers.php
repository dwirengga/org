<?php

if (! function_exists('successResponse')) {
    function successResponse($data,$message = 'success', int $code=200)
    {
        return response()->json([
        	'code' => $code, 
        	'message' => $message, 
        	'data' => $data
        ], $code);
    }
}

if (! function_exists('errorResponse')) {
    function errorResponse(string $message, $errors, int $code = 500)
    {
        $metadata = [
            'code' => $code,
            'message' => $message
        ];

        if ($errors) {
            $metadata['errors'] = $errors;
        }

        return response()->json($metadata, $code);
    }
}

if (!function_exists('paginationResponse')) {
    function paginationResponse($data,int $code = 200)
    {
        $response = [
            'code' => $code,
            'message' => 'success',
            'data' => [
                'page' => isset($data['page']) ? $data['page'] : $data->currentPage(),
                'limit' => isset($data['limit']) ? $data['limit'] : $data->perPage(),
                'total_items' => isset($data['total']) ? $data['total'] : $data->total(),
                'total_page' => isset($data['total_page']) ? $data['total_page'] : $data->lastPage(),
                'items' => isset($data['items']) ? $data['items'] : $data->items()
            ]
        ];
        return response()->json($response, $code);
    }
}
