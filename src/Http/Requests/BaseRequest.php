<?php

namespace DwiRengga\Org\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->sort_by == null) {
            $this->merge([
                'sort_by' => 'id',
            ]);
        }

        if ($this->sort_dir == null) {
            $this->merge([
                'sort_dir' => 'desc'
            ]);
        }

        if ($this->page == null) {
            $this->merge([
                'page' => 1
            ]);
        }

        if ($this->limit == null) {
            $this->merge([
                'limit' => 10
            ]);
        }
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'page' => 'numeric|min:1',
            'limit' => 'numeric|min:1',
            'search' => 'string|min:1',
            'sort_by' => 'nullable|string',
            'sort_dir' => 'nullable|string|in:desc,asc',
        ];
    }
}
