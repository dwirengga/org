<?php

namespace DwiRengga\Org\Http\Controllers;

use DwiRengga\Org\Http\Requests\DivisionRequest;
use DwiRengga\Org\Http\Requests\BaseRequest;
use DwiRengga\Org\Repositories\Eloquent\DivisionRepository;

class DivisionController extends Controller
{
    protected $divisionRepository;

    public function __construct(DivisionRepository $divisionRepository)
    {
        $this->divisionRepository = $divisionRepository;
    }

    public function index(BaseRequest $request)
    {
        $data = $this->divisionRepository->all($request);
        return paginationResponse($data);
    }

    public function store(DivisionRequest $request)
    {
        $data = $this->divisionRepository->create($request->all());
        return successResponse($data,null, 201);
    }

    public function show($id)
    {
        $data = $this->divisionRepository->find($id);
        return successResponse($data);
    }

    public function update(DivisionRequest $request, $id)
    {
        $data = $this->divisionRepository->update($id, $request->all());
        return successResponse($data);
    }

    public function destroy($id)
    {
        $this->divisionRepository->delete($id);
        return successResponse(null,'Division deleted successfully');
    }
}
