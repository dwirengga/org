<?php

namespace DwiRengga\Org\Http\Controllers;

use DwiRengga\Org\Http\Requests\CompanyRequest;
use DwiRengga\Org\Http\Requests\BaseRequest;
use DwiRengga\Org\Repositories\Eloquent\CompanyRepository;

class CompanyController extends Controller
{
    protected $companyRepository;

    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function index(BaseRequest $request)
    {
        $data = $this->companyRepository->all($request);
        return paginationResponse($data);
    }

    public function store(CompanyRequest $request)
    {
        $data = $this->companyRepository->create($request->all());
        return successResponse($data,null, 201);
    }

    public function show($id)
    {
        $data = $this->companyRepository->find($id);
        return successResponse($data);
    }

    public function update(CompanyRequest $request, $id)
    {
        $data = $this->companyRepository->update($id, $request->all());
        return successResponse($data);
    }

    public function destroy($id)
    {
        $this->companyRepository->delete($id);
        return successResponse(null,'Company deleted successfully');
    }
}
