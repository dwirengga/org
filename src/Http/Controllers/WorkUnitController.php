<?php

namespace DwiRengga\Org\Http\Controllers;

use DwiRengga\Org\Http\Requests\WorkUnitRequest;
use DwiRengga\Org\Http\Requests\BaseRequest;
use DwiRengga\Org\Repositories\Eloquent\WorkUnitRepository;

class WorkUnitController extends Controller
{
    protected $workUnitRepository;

    public function __construct(WorkUnitRepository $workUnitRepository)
    {
        $this->workUnitRepository = $workUnitRepository;
    }

    public function index(BaseRequest $request)
    {
        $data = $this->workUnitRepository->all($request);
        return paginationResponse($data);
    }

    public function store(WorkUnitRequest $request)
    {
        $data = $this->workUnitRepository->create($request->all());
        return successResponse($data,null, 201);
    }

    public function show($id)
    {
        $data = $this->workUnitRepository->find($id);
        return successResponse($data);
    }

    public function update(WorkUnitRequest $request, $id)
    {
        $data = $this->workUnitRepository->update($id, $request->all());
        return successResponse($data);
    }

    public function destroy($id)
    {
        $this->workUnitRepository->delete($id);
        return successResponse(null, 'Work Unit deleted successfully');
    }
}
