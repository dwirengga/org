<?php

namespace DwiRengga\Org\Http\Controllers;

use DwiRengga\Org\Http\Requests\PositionRequest;
use DwiRengga\Org\Http\Requests\BaseRequest;
use DwiRengga\Org\Repositories\Eloquent\PositionRepository;

class PositionController extends Controller
{
    protected $positionRepository;

    public function __construct(PositionRepository $positionRepository)
    {
        $this->positionRepository = $positionRepository;
    }

    public function index(BaseRequest $request)
    {
        $data = $this->positionRepository->all($request);
        return paginationResponse($data);
    }

    public function store(PositionRequest $request)
    {
        $data = $this->positionRepository->create($request->all());
        return successResponse($data,null, 201);
    }

    public function show($id)
    {
        $data = $this->positionRepository->find($id);
        return successResponse($data);
    }

    public function update(PositionRequest $request, $id)
    {
        $data = $this->positionRepository->update($id, $request->all());
        return successResponse($data);
    }

    public function destroy($id)
    {
        $this->positionRepository->delete($id);
        return successResponse(null, 'Position deleted successfully');
    }
}
