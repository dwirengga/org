<?php

return [
    'company' => [
        'table_name' => 'company',
        'enable' => true,
        'relation'=>[
            'work_unit'=>'n-1',
            'division'=>'n-1',
            'position'=>'n-1',
        ],
    ],
    'work_unit' => [
        'table_name' => 'work_unit',
        'enable' => true,
        'relation'=>[
            'company'=>'1-n',
        ],
    ],
    'division' => [
        'table_name' => 'division',
        'enable' => true,
    ],
    'position' => [
        'table_name' => 'position',
        'enable' => true,
    ],
];