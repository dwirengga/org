<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('struktur-organisasi.work_unit.table_name'), function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            if(config('struktur-organisasi.work_unit.leveling') == 'default'){
                $table->foreignId('parent_id')
                    ->nullable()
                    ->constrained(config('struktur-organisasi.work_unit.table_name'))
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->integer('level');
            }else{
                $table->foreignId('work_unit_1')
                    ->nullable()
                    ->constrained(config('struktur-organisasi.work_unit.table_name'))
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                
                $table->foreignId('work_unit_2')
                    ->nullable()
                    ->constrained(config('struktur-organisasi.work_unit.table_name'))
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                
                $table->foreignId('work_unit_3')
                    ->nullable()
                    ->constrained(config('struktur-organisasi.work_unit.table_name'))
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            }

            $table->timestamps();
            
            if(config('struktur-organisasi.company.enable')){
                $table->foreignId('company_id')
                    ->constrained(config('struktur-organisasi.company.table_name'))
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('struktur-organisasi.work_unit.table_name'));
    }
}