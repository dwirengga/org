<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDivisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('struktur-organisasi.division.table_name'), function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->timestamps();

            if(config('struktur-organisasi.company.enable')){
                $table->foreignId('company_id')
                    ->constrained(config('struktur-organisasi.company.table_name'))
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            }

            if(config('struktur-organisasi.work_unit.enable')){
                $table->foreignId('work_unit_id')
                    ->constrained(config('struktur-organisasi.work_unit.table_name'))
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('struktur-organisasi.division.table_name'));
    }
}